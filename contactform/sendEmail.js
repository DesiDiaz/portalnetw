// Client ID and API key from the Developer Console
var CLIENT_ID = '790072274229-4k8aqgsfv35dd2k15pul932qml9b6khm.apps.googleusercontent.com';
var apiKey = 'E7lwuizMsoZlWugssb8LfcJ_';

// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
//var SCOPES = 'https://www.googleapis.com/auth/gmail.readonly';
var SCOPES = 'https://www.googleapis.com/auth/gmail.readonly ' +
  'https://www.googleapis.com/auth/gmail.send';


var authorizeButton = document.getElementById('authorize-button');
var signoutButton = document.getElementById('signout-button');

/**
 *  On load, called to load the auth2 library and API client library.
 */
function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

/**
 *  Initializes the API client library and sets up sign-in state
 *  listeners.
 */
function initClient() {
  gapi.client.init({
    discoveryDocs: DISCOVERY_DOCS,
    clientId: CLIENT_ID,
    scope: SCOPES
  }).then(function () {
    // Listen for sign-in state changes.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

    // Handle the initial sign-in state.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    authorizeButton.onclick = handleAuthClick;
    signoutButton.onclick = handleSignoutClick;
  });
}

/**
 *  Called when the signed in status changes, to update the UI
 *  appropriately. After a sign-in, the API is called.
 */
function updateSigninStatus(isSignedIn) {
  if (isSignedIn) {
    authorizeButton.style.display = 'none';
    signoutButton.style.display = 'block';
    listLabels();
  } else {
    authorizeButton.style.display = 'block';
    signoutButton.style.display = 'none';
  }
}

/**
 *  Sign in the user upon button click.
 */
function handleAuthClick(callback) {
  gapi.auth2.getAuthInstance().signIn();
  callback();
}

/**
 *  Sign out the user upon button click.
 */
function handleSignoutClick(event) {
  gapi.auth2.getAuthInstance().signOut();
}

/**
 * Append a pre element to the body containing the given message
 * as its text node. Used to display the results of the API call.
 *
 * @param {string} message Text to be placed in pre element.
 */
function appendPre(message) {
  var pre = document.getElementById('content');
  var textContent = document.createTextNode(message + '\n');
  pre.appendChild(textContent);
}

/**
 * Print all Labels in the authorized user's inbox. If no labels
 * are found an appropriate message is printed.
 */
function listLabels() {
  gapi.client.gmail.users.labels.list({
    'userId': 'me'
  }).then(function (response) {
    var labels = response.result.labels;
    appendPre('Labels:');

    if (labels && labels.length > 0) {
      for (i = 0; i < labels.length; i++) {
        var label = labels[i];
        appendPre(label.name)
      }
    } else {
      appendPre('No Labels found.');
    }
  });
}

function sendMessage(userId, email, callback) {
  // Using the js-base64 library for encoding:
  // https://www.npmjs.com/package/js-base64
  var base64EncodedEmail = Base64.encodeURI(email);
  var request = gapi.client.gmail.users.messages.send({
    'userId': userId,
    'resource': {
      'raw': base64EncodedEmail
    }
  });
  request.execute(callback);
}

function sendEmail() {
  handleAuthClick(function () {
    setTimeout(() => {
      var nombre = 'Nombre: ';
      var correo = 'Correo electrónico: ';
      var asunto = 'Asunto: ';
      var mensaje = 'Mensaje: ';
      debugger;
      if (document.getElementById("nombreremitente") != null) {
        var nameInput = document.getElementById("nombreremitente").value;
      }
      if (document.getElementById("correoremitente") != null) {
        var emailInput = document.getElementById("correoremitente").value;
      }
      if (document.getElementById("asunto") != null) {
        var subjectInput = document.getElementById("asunto").value;
      }
      if (document.getElementById("mensaje") != null) {
        var msgInput = document.getElementById("mensaje").value;
      }
      var resultInput = "\n " + nombre + nameInput + "\n " + correo + emailInput + "\n " + asunto + subjectInput + "\n " + mensaje + msgInput;
      sendMessage(
        {
          'To': document.getElementById("email-to").value,//Aqui va el correo de NETW
          'Subject': document.getElementById("email-subject").value
        },
        resultInput,
        // attachments: [convert( document.body ).to("screenshot.png"),
        //   { filename: "info.text", data: "Some info about the page"}
        // ],
        composeTidy
      );
    }, 2000);


  });


  return false;
}

function sendMessage(headers_obj, message, callback) {
  debugger;
  var email = '';

  for (var header in headers_obj)
    email += header += ": " + headers_obj[header] + "\r\n";

  email += "\r\n" + message;

  // email += "\r\n" + attachments;

  var sendRequest = gapi.client.gmail.users.messages.send({
    'userId': 'me',
    'resource': {
      'raw': window.btoa(email).replace(/\+/g, '-').replace(/\//g, '_')
    }
  });

  return sendRequest.execute(callback);
}


function composeTidy() {
  alert("El mensaje fue enviado correctamente a NetW Consulting");
}
