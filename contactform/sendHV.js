// Client ID and API key from the Developer Console
var CLIENT_ID = '790072274229-4k8aqgsfv35dd2k15pul932qml9b6khm.apps.googleusercontent.com';
var apiKey = 'E7lwuizMsoZlWugssb8LfcJ_';

// Array of API discovery doc URLs for APIs used by the quickstart
var DISCOVERY_DOCS = ["https://www.googleapis.com/discovery/v1/apis/gmail/v1/rest"];

// Authorization scopes required by the API; multiple scopes can be
// included, separated by spaces.
//var SCOPES = 'https://www.googleapis.com/auth/gmail.readonly';
var SCOPES = 'https://www.googleapis.com/auth/gmail.readonly ' +
  'https://www.googleapis.com/auth/gmail.send';


var authorizeButton = document.getElementById('authorize-button');
var signoutButton = document.getElementById('signout-button');

/**
 *  On load, called to load the auth2 library and API client library.
 */
function handleClientLoad() {
  gapi.load('client:auth2', initClient);
}

/**
 *  Initializes the API client library and sets up sign-in state
 *  listeners.
 */
function initClient() {
  gapi.client.init({
    discoveryDocs: DISCOVERY_DOCS,
    clientId: CLIENT_ID,
    scope: SCOPES
  }).then(function () {
    // Listen for sign-in state changes.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus);

    // Handle the initial sign-in state.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get());
    authorizeButton.onclick = handleAuthClick;
    signoutButton.onclick = handleSignoutClick;
  });
}

/**
 *  Called when the signed in status changes, to update the UI
 *  appropriately. After a sign-in, the API is called.
 */
function updateSigninStatus(isSignedIn) {
  debugger;
  if (isSignedIn) {
    authorizeButton.style.display = 'none';
    signoutButton.style.display = 'block';
    listLabels();
  } else {
    authorizeButton.style.display = 'block';
    signoutButton.style.display = 'none';
  }
}

/**
 *  Sign in the user upon button click.
 */
function handleAuthClick(callback) {
  gapi.auth2.getAuthInstance().signIn();
  callback();
}

/**
 *  Sign out the user upon button click.
 */
function handleSignoutClick(event) {
  gapi.auth2.getAuthInstance().signOut();
}

/**
 * Append a pre element to the body containing the given message
 * as its text node. Used to display the results of the API call.
 *
 * @param {string} message Text to be placed in pre element.
 */
function appendPre(message) {
  var pre = document.getElementById('content');
  var textContent = document.createTextNode(message + '\n');
  pre.appendChild(textContent);
}

/**
 * Print all Labels in the authorized user's inbox. If no labels
 * are found an appropriate message is printed.
 */
function listLabels() {
  gapi.client.gmail.users.labels.list({
    'userId': 'me'
  }).then(function (response) {
    var labels = response.result.labels;
    appendPre('Labels:');

    if (labels && labels.length > 0) {
      for (i = 0; i < labels.length; i++) {
        var label = labels[i];
        appendPre(label.name)
      }
    } else {
      appendPre('No Labels found.');
    }
  });
}

function sendMessage(userId, email, callback) {
  // Using the js-base64 library for encoding:
  // https://www.npmjs.com/package/js-base64
  var base64EncodedEmail = Base64.encodeURI(email);
  var request = gapi.client.gmail.users.messages.send({
    'userId': userId,
    'resource': {
      'raw': base64EncodedEmail
    }
  });
  request.execute(callback);
}

function sendHV() {
  handleAuthClick(function () {
    debugger;


    setTimeout(() => {
      var nombre = 'Nombre: ';
      var email = 'Correo electrónico: ';
      var telefono = 'Teléfono: ';
      var mensaje = 'Comentarios: ';
      if (document.getElementById("nombre") != null) {
        var nameInput = document.getElementById("nombre").value;
      }
      if (document.getElementById("correo") != null) {
        var emailInput = document.getElementById("correo").value;
      }
      if (document.getElementById("telefono") != null) {
        var telefonoInput = document.getElementById("telefono").value;
      }
      if (document.getElementById("comentario") != null) {
        var msgInput = document.getElementById("comentario").value;
      }
      var resultInput = nombre + nameInput + "\n " + email + emailInput + "\n " + telefono + telefonoInput + "\n " + mensaje + msgInput;
      sendMessage(
        {
          'To': document.getElementById("email-to").value,//Aqui va el correo de NETW
          'Subject': document.getElementById("email-subject").value
        },
        resultInput,
        composeTidy
      );
    }, 2000);


  });


  return false;
}

function sendMessage(headers_obj, message, callback) {
  debugger;
  var email = '';

  for (var header in headers_obj)
    email += header += ": " + headers_obj[header] + "\r\n";

  email += "\r\n" + message;

  // email += "\r\n" + attachments;

  var sendRequest = gapi.client.gmail.users.messages.send({
    'userId': 'me',
    'resource': {
      'raw': window.btoa(email).replace(/\+/g, '-').replace(/\//g, '_')
    }
  });

  return sendRequest.execute(callback);
}


function composeTidy() {
  alert("El mensaje fue enviado correctamente a Netw Consulting");
}


// ejemplo 
// Get the canvas from the DOM and turn it into base64-encoded png data.
var canvas = document.getElementById("canvas");
var dataUrl = canvas.toDataURL();

// The relevant data is after 'base64,'.
var pngData = dataUrl.split('base64,')[1];

// Put the data in a regular multipart message with some text.
var mail = [
  'Content-Type: multipart/mixed; boundary="foo_bar_baz"\r\n',
  'MIME-Version: 1.0\r\n',
  'From: sender@gmail.com\r\n',
  'To: receiver@gmail.com\r\n',
  'Subject: Subject Text\r\n\r\n',

  '--foo_bar_baz\r\n',
  'Content-Type: text/plain; charset="UTF-8"\r\n',
  'MIME-Version: 1.0\r\n',
  'Content-Transfer-Encoding: 7bit\r\n\r\n',

  'The actual message text goes here\r\n\r\n',

  '--foo_bar_baz\r\n',
  'Content-Type: image/png\r\n',
  'MIME-Version: 1.0\r\n',
  'Content-Transfer-Encoding: base64\r\n',
  'Content-Disposition: attachment; filename="example.png"\r\n\r\n',

   pngData, '\r\n\r\n',

   '--foo_bar_baz--'
].join('');

// enviar el correo
$.ajax({
  type: "POST",
  url: "https://www.googleapis.com/upload/gmail/v1/users/CLIENT_ID/messages/send?uploadType=media",
  contentType: "message/rfc822",
  beforeSend: function(xhr, settings) {
    xhr.setRequestHeader('Authorization','Bearer ' + apiKey);
  },
  data: mail
}); 




// window.onload = function () {
//   var app = function () {
//     // var baseUrl = 'http://127.0.0.1:5984/playground/';
//     var fileInput = document.forms['upload'].elements['fileup'];
//     document.forms['upload'].onsubmit = function () {
//       uploadFile('foo', fileInput.files[0]);
//       return false;
//     };
// debugger;
//     var uploadFile = function (docName, file) {
//       var name = encodeURIComponent(file.name),
//         type = file.type,
//         fileReader = new FileReader(),
//         getRequest = new XMLHttpRequest(),
//         putRequest = new XMLHttpRequest();

//       getRequest.open('GET', encodeURIComponent(docName),
//         true);
//       getRequest.send();
//       getRequest.onreadystatechange = function (response) {
//         if (getRequest.readyState == 4 && getRequest.status == 200) {
//           var doc = JSON.parse(getRequest.responseText);
//           putRequest.open('PUT', baseUrl +
//             encodeURIComponent(docName) + '/' +
//             name + '?rev=' + doc._rev, true);
//           putRequest.setRequestHeader('Content-Type', type);
//           fileReader.readAsArrayBuffer(file);
//           fileReader.onload = function (readerEvent) {
//             putRequest.send(readerEvent.target.result);
//           };
//           putRequest.onreadystatechange = function (response) {
//             if (putRequest.readyState == 4) {
//               console.log(putRequest);
//             }
//           };
//         }
//       };
//     };
//   };
//   app();
// };
